/*
 * @Author: xuhy
 * @Date: 2022-04-03 16:19:21
 * @LastEditTime: 2022-04-19 10:29:35
 * @LastEditors: Please set LastEditors
 * @Description: vue配置项
 * @FilePath: \vue_shop\vue.config.js
 */
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})
