/*
 * @Author: your name
 * @Date: 2022-04-03 16:19:21
 * @LastEditTime: 2022-04-06 21:19:41
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue_shop\vue.config.js
 */
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  Server: {
    open: true,
    host: 'localhost',
    post: 8080
  }
})
