/*
 * @Author: xuhy
 * @Date: 2022-04-03 16:21:52
 * @LastEditTime: 2022-04-03 23:05:17
 * @LastEditors: Please set LastEditors
 * @Description: 按需导入element-ui组件
 * @FilePath: \vue_shop\src\plugins\element.js
 */
import Vue from 'vue'
import { Button, Form, FormItem, Input, Message } from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.prototype.$message = Message
