/*
 * @Author: xuhy
 * @Date: 2022-04-03 16:21:52
 * @LastEditTime: 2022-04-04 09:33:13
 * @LastEditors: Please set LastEditors
 * @Description: 按需导入element-ui组件
 * @FilePath: \vue_shop\src\plugins\element.js
 */
import Vue from 'vue'
import { Button, Form, FormItem, Input, Message, Container, Header, Aside, Main } from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.prototype.$message = Message
