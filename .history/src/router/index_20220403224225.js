/*
 * @Author: your name
 * @Date: 2022-04-03 16:19:21
 * @LastEditTime: 2022-04-03 22:42:25
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue_shop\src\router\index.js
 */
import Vue from 'vue'
import Router from 'vue-router'
import LoginUser from '../components/LoginUser'
import HomeAccess from '../components/HomeAccess'

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: LoginUser },
    { path: '/home', component: HomeAccess }
  ]
})
export default router
