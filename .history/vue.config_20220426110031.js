/*
 * @Author: xuhy
 * @Date: 2022-04-03 16:19:21
 * @LastEditTime: 2022-04-26 11:00:31
 * @LastEditors: Please set LastEditors
 * @Description: webpack配置文件
 * @FilePath: \vue_shop\vue.config.js
 */
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})
