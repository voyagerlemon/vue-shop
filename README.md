<!--
 * @Author: xuhy
 * @Date: 2022-04-03 16:19:30
 * @LastEditTime: 2022-04-26 17:22:38
 * @LastEditors: Please set LastEditors
 * @Description: README文件
 * @FilePath: \vue_shop\README.md
-->
# 电商后台管理系统

## 开发工具
- nodejs >= 10
- vscode（插件安装 vetur、eslint、prettier）
- yarn (不能用 npm 安装依赖)


## 目录组织规范
```
├── public
│ ├── favicon.ico
│ ├── index.html
├── src
│ ├── App.vue --------------------------- Root组件
│ ├── assets ---------------------------- 静态文件，一般用来放图片
│ ├── components ------------------------ 全局组件
│ ├── main-dev.js --------------------------- app 开发入口
│ ├── main-prod.js --------------------------- app 打包入口
├── vue_api_server --------------------------- 后台数据服务器
├── vue.config.js
├── babel.config.js
├── jsconfig.json
├── package.json
├── .prettierrc
├── .eslintrc
├── yarn.lock
```

## Install yarn
```
npm install --global yarn

```
## Check installation
```
yarn --version

```

## Project setup
```
yarn install
```
### Compiles and hot-reloads for development
```
yarn serve
```
### Compiles and minifies for production
```
yarn build
```
### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
